# assignment1

## Add your files
commands:

```
cd existing_repo
git clone https://gitlab.com/worldline1072407/assignment1.git
git init
git add .
git status
git commit -m "New files added"
git remote add origin https://gitlab.com/worldline1072407/assignment1.git
git branch -M main
git push -uf origin main
```

# Deploying on gitlab

- configure .gitlab-ci.yml file using node image
- stages: order of job execution
- artifacts: list of files and repository to attach to file
- image: to specify a docker image that the job runs in

## Prerequisites: 
- node.js
- git commands
- gitlab
