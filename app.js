const express = require('express');
const bodyParser = require('body-parser');
const fs = require('fs/promises');
const path = require('path');

const app = express();
const port = 3000;

app.use(express.static('public'));
app.use(bodyParser.urlencoded({ extended: true }));

const usersDirPath = path.join(__dirname, 'users');

app.get('/', (req,res) => {
  res.sendFile(path.join(__dirname, 'public', 'home.html'));
})
app.get('/login', (req, res) => {
  res.sendFile(path.join(__dirname, 'public', 'login.html'));
});

app.get('/signup', (req, res) => {
  res.sendFile(path.join(__dirname, 'public', 'signup.html'));
});

app.post('/login', async (req, res) => {
  const { username, password } = req.body;
  const userFilePath = path.join(usersDirPath, `${username}.json`);

  try {
    const user = await getUser(userFilePath);
    if (user && user.password === password) {
      res.send(`Welcome, ${username}!`);
    } else {
      res.send('Invalid login credentials.');
    }
  } catch (error) {
    res.send('Invalid login credentials.');
  }
});

app.post('/signup', async (req, res) => {
  const { username, password } = req.body;
  const userFilePath = path.join(usersDirPath, `${username}.json`);

  try {
    await getUser(userFilePath);
    res.send('Username already taken. Please choose another.');
  } catch (error) {
    // If the file doesn't exist, user doesn't exist, proceed with signup
    const user = { username, password };
    await saveUser(userFilePath, user);
    res.send(`User ${username} created successfully!`);
  }
});

async function getUser(userFilePath) {
  const content = await fs.readFile(userFilePath, 'utf-8');
  return JSON.parse(content);
}

async function saveUser(userFilePath, user) {
  const userJSON = JSON.stringify(user, null, 2);
  await fs.writeFile(userFilePath, userJSON);
}

app.listen(port, () => {
  console.log(`Server is running at http://localhost:${port}`);
});
